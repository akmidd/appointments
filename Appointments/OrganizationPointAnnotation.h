//
//  OrganizationPointAnnotation.h
//  Appointments
//
//  Created by admin on 28/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import <MapKit/MapKit.h>

@interface OrganizationPointAnnotation : MKPointAnnotation

@property(nonatomic,readonly) NSInteger organizationId;

-(instancetype) initWithOrganizationId:(NSInteger)organizationId;

@end
