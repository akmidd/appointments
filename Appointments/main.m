//
//  main.m
//  Appointments
//
//  Created by admin on 27/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
