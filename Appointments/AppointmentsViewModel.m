//
//  AppointmentsViewModel.m
//  Appointments
//
//  Created by admin on 28/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import "AppointmentsViewModel.h"
#import "AppointmentsDataSource.h"



@implementation AppointmentsViewModel {
    AppointmentsDataSource * _dataSource;
}

-(instancetype) init {
    if(self = [super init]) {
        _dataSource = [[AppointmentsDataSource alloc] init];
    }
    return self;
}

-(void) loadDataFromMock {
    Organization* organization1 = [[Organization alloc] initWithId:100 withTitle:@"Тинькофф-Банк" withLatitude:55.836226 withLongitude:37.483064];
    Organization* organization2 = [[Organization alloc] initWithId:101 withTitle:@"Сбербанк-Технологии" withLatitude:55.696494 withLongitude:37.625472];
    
    
    self.organizations = @[
                        organization1,
                        organization2];
    
    self.appointments = @[
                       [[Appointment alloc] initWithTitle:@"Встреча в компании" withOrganization:organization1],
                       [[Appointment alloc] initWithTitle:@"Презентация в компании" withOrganization:organization2],
                       [[Appointment alloc] initWithTitle:@"Собеседование" withOrganization:organization2]];
    
    self.selectedAppointmentIndex = 0;
    
}

-(void) refresh {
    //[self loadDataFromMock];
    
    [_dataSource loadDataFromServerWithCompletionHandler:^(NSArray<Appointment *> *appointments, NSArray<Organization *> *organizations, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            _organizations = organizations;
            self.appointments = appointments;
            self.selectedAppointmentIndex = 0;
        });

    }];
    
}

-(Appointment*) selectedAppointment {
    return [_appointments objectAtIndex:_selectedAppointmentIndex];
}

-(NSInteger) numberOfAppointementsInOrganization:(NSInteger) organizationId {
    NSInteger appCount = 0;
    
    for (Appointment * app in _appointments) {
        if (app.organization.identifier == organizationId) {
            appCount += 1;
        }
    }
    
    return appCount;
    
}

-(void) selectFirstAppointmentForOrganizationId:(NSInteger) organizationId {
    [_appointments enumerateObjectsUsingBlock:^(Appointment * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.organization.identifier == organizationId) {
            self.selectedAppointmentIndex = idx;
            *stop = YES;
        }
    }];
}




@end
