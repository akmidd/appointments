//
//  OrganizationPointAnnotation.m
//  Appointments
//
//  Created by admin on 28/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import "OrganizationPointAnnotation.h"

@implementation OrganizationPointAnnotation

-(instancetype) initWithOrganizationId:(NSInteger)organizationId {
    if(self = [super init]) {
        _organizationId = organizationId;
        return self;
    }
    
    return nil;
}

@end
