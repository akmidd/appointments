//
//  AppDelegate.h
//  Appointments
//
//  Created by admin on 27/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

