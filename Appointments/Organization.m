//
//  Organization.m
//  Appointments
//
//  Created by admin on 28/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import "Organization.h"

@implementation Organization

-(instancetype) initWithId:(NSInteger*) identifier withTitle:(NSString*) title withLatitude:(double) latitude withLongitude:(double) longitude {
    if (!(self = [super init])) {
        return nil;
    }
    
    _identifier = identifier;
    _title = title;
    _latitude = latitude;
    _longitude = longitude;
    
    return self;
}

@end
