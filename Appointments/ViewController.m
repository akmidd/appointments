//
//  ViewController.m
//  Appointments
//
//  Created by admin on 27/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>
#import "AppointmentsViewModel.h"
#import "OrganizationPointAnnotation.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@end

@implementation ViewController {
    AppointmentsViewModel * _viewModel;
}

NSString * const cellId = @"cellId";


- (void)viewDidLoad {
    [super viewDidLoad];

    _viewModel = [[AppointmentsViewModel alloc] init];
    
    [_viewModel addObserver:self forKeyPath:@"appointments" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
    [_viewModel addObserver:self forKeyPath:@"selectedAppointmentIndex" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.tableFooterView = [[UIView alloc] init];
    _tableView.tableFooterView.backgroundColor = [UIColor lightGrayColor];
    
    _mapView.delegate = self;
    
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"appointments"]) {
        [self updateAppointmentsTable];
        [self updateAppointmentsMarkersOnMap];
        
    } else if ([keyPath isEqualToString:@"selectedAppointmentIndex"]) {
        [self updateSelectedAppointmentInTable];
        [self updateSelectedMarkerOnMap];
    }
}


-(void) dealloc {
    [_viewModel removeObserver:self forKeyPath:@"appointments"];
    [_viewModel removeObserver:self forKeyPath:@"selectedAppointmentIndex"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [_viewModel refresh];
}

- (IBAction)refreshButtonTapped:(id)sender {
    [_viewModel refresh];
}


#pragma mark - Map related methods

-(void) updateAppointmentsMarkersOnMap {
    [_mapView removeAnnotations:_mapView.annotations];

    for (Organization* organization in _viewModel.organizations) {
        
        CLLocationCoordinate2D coordinates;
        coordinates.latitude = organization.latitude;
        coordinates.longitude = organization.longitude;
        
        OrganizationPointAnnotation *annotation = [[OrganizationPointAnnotation alloc]initWithOrganizationId:organization.identifier];
        annotation.coordinate = coordinates;
        annotation.title = [NSString stringWithFormat:@"%@ (кол-во встреч: %d)", organization.title, [_viewModel numberOfAppointementsInOrganization:organization.identifier]];
        
        [_mapView addAnnotation:annotation];
        
        if (organization.identifier == _viewModel.selectedAppointment.organization.identifier) {
            [_mapView setCenterCoordinate:coordinates animated:YES];
        }
    }
    

}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    OrganizationPointAnnotation * orgAnnotation = (OrganizationPointAnnotation *) annotation;

    MKPinAnnotationView *pinView = nil;
    
    static NSString *defaultPinID = @"organizationPinId";
    pinView = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
    if ( pinView == nil ) {
        pinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID] ;
    }
    
    pinView.canShowCallout = YES;
    if (orgAnnotation.organizationId == _viewModel.selectedAppointment.organization.identifier) {
        pinView.pinTintColor = [UIColor greenColor];
        [_mapView setCenterCoordinate:orgAnnotation.coordinate animated:YES];
    } else {
        pinView.pinTintColor = [UIColor lightGrayColor];
    }

    
    return pinView;
}

- (void) updateSelectedMarkerOnMap {
    for (OrganizationPointAnnotation * orgAnotation in _mapView.annotations) {
        MKPinAnnotationView * annView = (MKPinAnnotationView *) [_mapView viewForAnnotation:orgAnotation];
        if (annView) {
            if (orgAnotation.organizationId == _viewModel.selectedAppointment.organization.identifier) {
                annView.pinTintColor = [UIColor greenColor];
                [_mapView setCenterCoordinate:orgAnotation.coordinate animated:YES];
            } else {
                annView.pinTintColor = [UIColor lightGrayColor];
            }
        }
    }
}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    OrganizationPointAnnotation * orgPointAnnotation = (OrganizationPointAnnotation *) view.annotation;
    
    [_viewModel selectFirstAppointmentForOrganizationId:orgPointAnnotation.organizationId];
}

-(MKPinAnnotationView*) createMarkerWithLocation: (CLLocationCoordinate2D) location andTitle: (NSString*) title andColor: (UIColor*) color{
    MKPointAnnotation *resultPin = [[MKPointAnnotation alloc] init];
    MKPinAnnotationView *result = [[MKPinAnnotationView alloc] initWithAnnotation:resultPin reuseIdentifier:Nil];
    [resultPin setCoordinate:location];
    resultPin.title = title;
    result.pinTintColor = color;
    return result;
}


#pragma mark - Table related methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _viewModel.appointments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellId];
    
    
    Appointment * appointment = [_viewModel.appointments objectAtIndex:indexPath.row];
    
    
    cell.textLabel.text = appointment.title;
    cell.detailTextLabel.text = appointment.organization.title;
    
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _viewModel.selectedAppointmentIndex = indexPath.row;
}

-(void) updateAppointmentsTable {
    [_tableView reloadData];
}

-(void) updateSelectedAppointmentInTable {
    [_tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_viewModel.selectedAppointmentIndex inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
}

#pragma mark

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
