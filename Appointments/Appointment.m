//
//  Appointment.m
//  Appointments
//
//  Created by admin on 28/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import "Appointment.h"

@implementation Appointment


-(instancetype) initWithTitle:(NSString*)title withOrganization:(Organization*)organization {
    if (!(self = [super init])) {
        return nil;
    }
    
    _title = title;
    _organization = organization;
    return self;
}
@end
