//
//  AppointmentsDataSource.h
//  Appointments
//
//  Created by admin on 28/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Appointment.h"
#import "Organization.h"

@interface AppointmentsDataSource : NSObject

-(void) loadDataFromServerWithCompletionHandler:(void (^)(NSArray<Appointment *> * appointments, NSArray<Organization *> * organizations, NSError *error)) completionHandler;

@end
