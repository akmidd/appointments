//
//  Appointment.h
//  Appointments
//
//  Created by admin on 28/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Organization;

@interface Appointment : NSObject
@property(nonatomic, readonly) NSString* title;
@property(nonatomic, readonly) Organization* organization;


-(instancetype) initWithTitle:(NSString*)title withOrganization:(Organization*)organization ;

@end
