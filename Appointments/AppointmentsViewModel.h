//
//  AppointmentsViewModel.h
//  Appointments
//
//  Created by admin on 28/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Appointment.h"
#import "Organization.h"

@interface AppointmentsViewModel : NSObject

@property(nonatomic) NSArray<Appointment*>* appointments;
@property(nonatomic) NSArray<Organization*>* organizations;
@property(nonatomic, readonly) Appointment* selectedAppointment;
@property(nonatomic) NSInteger selectedAppointmentIndex;

-(instancetype) init;

// loads data from the server
-(void) refresh;
-(NSInteger) numberOfAppointementsInOrganization:(NSInteger) organizationId;
-(void) selectFirstAppointmentForOrganizationId:(NSInteger) organizationId;


@end
