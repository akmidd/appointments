//
//  ViewController.h
//  Appointments
//
//  Created by admin on 27/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate>


@end

