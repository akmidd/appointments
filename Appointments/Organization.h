//
//  Organization.h
//  Appointments
//
//  Created by admin on 28/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Organization : NSObject

@property(nonatomic, readonly) NSInteger identifier;
@property(nonatomic, readonly) NSString * title;
@property(nonatomic, readonly) double latitude;
@property(nonatomic, readonly) double longitude;


-(instancetype) initWithId:(NSInteger) identifier withTitle:(NSString*) title withLatitude:(double) latitude withLongitude:(double) longitude;

@end
