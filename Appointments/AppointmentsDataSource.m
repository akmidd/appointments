//
//  AppointmentsDataSource.m
//  Appointments
//
//  Created by admin on 28/03/17.
//  Copyright © 2017 Dmitry Klochkov. All rights reserved.
//

#import "AppointmentsDataSource.h"
#import <UIKit/UIKit.h>

@implementation AppointmentsDataSource


-(void) loadDataFromServerWithCompletionHandler:(void (^)(NSArray<Appointment *> * appointments, NSArray<Organization *> * organizations, NSError *error)) completionHandler {
    
    NSArray<Appointment *> __block * appointments = nil;
    NSDictionary __block * organizationsByIdDict = nil;

    
     NSURLSessionDataTask *task1 = [[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:@"http://demo3526062.mockable.io/getOrganizationListTest"] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSMutableDictionary* details = [NSMutableDictionary dictionary];
            [details setValue:@"Failed to load data from server" forKey:NSLocalizedDescriptionKey];
            NSError * resultError = [NSError errorWithDomain:@"dmitry.klovhkov.appointments" code:1 userInfo:details];
            completionHandler(nil, nil, resultError);
        } else {
            organizationsByIdDict = [self deserializeOrganizationsJson:[NSJSONSerialization JSONObjectWithData:data options:0 error:nil]];
            NSURLSessionDataTask *task2 = [[NSURLSession sharedSession] dataTaskWithURL:[NSURL URLWithString:@"http://demo3526062.mockable.io/getVisitsListTest"] completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                if (error) {
                    NSMutableDictionary* details = [NSMutableDictionary dictionary];
                    [details setValue:@"Failed to load data from server" forKey:NSLocalizedDescriptionKey];
                    NSError * resultError = [NSError errorWithDomain:@"dmitry.klovhkov.appointments" code:1 userInfo:details];
                    completionHandler(nil, nil, resultError);
                } else {
                    appointments = [self deserializeAppointmentsJson: [NSJSONSerialization JSONObjectWithData:data options:0 error:nil] withOrganizations:organizationsByIdDict];
                    completionHandler(appointments, [organizationsByIdDict allValues], nil);
                }
                
            }];
            [task2 resume];
        }
    }];
    
    [task1 resume];

}


-(NSArray<Appointment *> *) deserializeAppointmentsJson:(NSArray *) jsonArray withOrganizations:(NSDictionary *) organizationDict {
    NSMutableArray<Appointment *> * appointments = [NSMutableArray new];
    
    for (NSDictionary * appJson in jsonArray) {
        
        Organization * organization = [organizationDict valueForKey:[appJson valueForKey:@"organizationId"]];
        
        [appointments addObject:[[Appointment alloc] initWithTitle:[appJson valueForKey:@"title"] withOrganization: organization]];
    }
    
    return appointments;
}

-(NSDictionary *) deserializeOrganizationsJson:(NSArray *) jsonArray {
    NSMutableDictionary * dict = [NSMutableDictionary new];
    
    for (NSDictionary * json in jsonArray) {
        Organization * organization = [[Organization alloc]
                                       initWithId:[[json valueForKey:@"organizationId"] integerValue]
                                       withTitle:[json valueForKey:@"title"]
                                       withLatitude:[[json valueForKey:@"latitude"] doubleValue]
                                       withLongitude:[[json valueForKey:@"longitude"] doubleValue]];
        
        [dict setObject:organization forKey:[@(organization.identifier) stringValue]];
        
    }

    return dict;
}

        
        
@end
